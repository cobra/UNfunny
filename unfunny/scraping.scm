;; Copyright (C) 2024 Skylar Astaroth <cobra@vern.cc>
;;
;; This file is part of UNfunny
;;
;; UNfunny is free software: you can redistribute it and/or modify it under the
;; terms of the GNU Affero General Public License as published by the Free
;; Software Foundation, either version 3 of the License, or (at your option) any
;; later version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
;; for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (unfunny scraping)
  #:use-module (unfunny vips)
  #:use-module (libxml2)
  #:use-module (system foreign)
  #:use-module (rnrs)
  #:use-module (ice-9 string-fun)
  #:use-module (ice-9 binary-ports)
  #:export (proxy crop get-picture))

(define (proxy url)
  (string-append "/proxy?url=" url))

(define (crop image)
  (if (not (file-exists? "/tmp/unfunny"))
    (mkdir "/tmp/unfunny"))
  (define in (vips-image-new-from-buffer
               (bytevector->pointer image)
               (array-length image)
               (string->pointer "")
               %null-pointer))
  (define out (bytevector->pointer (make-bytevector 0)))
  (vips-extract-area in out 0 0 (vips-image-get-width in)
                     (- (vips-image-get-height in) 20) %null-pointer)
  (define buf (bytevector->pointer (make-bytevector 0)))
  (define len (make-bytevector (sizeof size_t)))
  (vips-pngsave-buffer (dereference-pointer out)
                       buf (bytevector->pointer len) %null-pointer)
  (pointer->bytevector (dereference-pointer buf)
                       (bytevector-u64-native-ref len 0)))

(define (get-picture d)
  (let ((pref "/html/body/div/div[2]/div[2]/div/div[1]/div[1]/img/@"))
    (list
      (get-xpath-string (string-append pref "src") d #t)
      (get-xpath-string (string-append pref "alt") d #t))))
